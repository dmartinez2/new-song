//done by:
//David Martinez NetID: dam364
//Justin Hinds NetID: jth140

package view;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;

public class ListController {
	@FXML
	Button addButton;
	
	@FXML
	Button editButton;
	
	@FXML
	Button deleteButton;
	
	@FXML
	Label songNameLabel;
	
	@FXML
	Label songArtistLabel;
	
	@FXML
	Label songAlbumLabel;
	
	@FXML
	Label songYearLabel;
	
	@FXML         
	ListView<Song> listView;
	
	@FXML
	TextField nameField;
	
	@FXML
	TextField artistField;
	
	@FXML
	TextField albumField;
	
	@FXML
	TextField yearField;
	
	@FXML
	Label modeLabel;
	
	@FXML
	Button doneButton;
	
	@FXML
	Button cancelButton;

	private ObservableList<Song> obsList = FXCollections.emptyObservableList();              

	public void start(Stage mainStage) {                
		// create an ObservableList 
		// from an ArrayList  
		try {
			readJSON();
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		listView.setItems(obsList.sorted()); 
		
	    // set listener for the items
	    listView
	    .getSelectionModel()
	    .selectedItemProperty()
	    .addListener((obs, oldVal, newVal) -> setLabels());
	    
	    // select the first item
	    if (obsList.isEmpty()) {
	    	emptyProcedure();
	    }
	    listView.getSelectionModel().select(0);
	      
	    nameField.setDisable(true);
	    artistField.setDisable(true);
	    albumField.setDisable(true);
	    yearField.setDisable(true);
	      
	    doneButton.setDisable(true);
	    cancelButton.setDisable(true);
	      
	    addButton.setOnAction(e -> addProcedure(mainStage));
	    editButton.setOnAction(e -> editProcedure(mainStage));
	    deleteButton.setOnAction(e -> showDeleteDialog(mainStage));
	}
	
	private void emptyProcedure() {
		editButton.setDisable(true);
		deleteButton.setDisable(true);
		setLabels();
	}
	
	private void setLabels(){
		
		if(!(obsList.isEmpty())) {
			//for the first song you add
			//if you try to edit that first song a NULLpointerException is thrown
			//this will catch it
			try {
			Song selectedSong = listView.getSelectionModel().getSelectedItem();
			songNameLabel.setText(selectedSong.getName());
			songArtistLabel.setText(selectedSong.getArtist());
			songAlbumLabel.setText(selectedSong.getAlbum());
			songYearLabel.setText(selectedSong.getYear());
			} catch (Exception e) {
				
			}
		} else {
			songNameLabel.setText("");
			songArtistLabel.setText("");
			songAlbumLabel.setText("");
			songYearLabel.setText("");
		}
	}
	
	//write JSON file
	private void writeJSON() throws IOException {
		Gson gson = new Gson();
		String json = gson.toJson(obsList);
		FileWriter fileWriter = new FileWriter("songs.json");
		PrintWriter printWriter = new PrintWriter(fileWriter);
		printWriter.print(json);
		printWriter.close();
	}
	
	//read from json file
	private void readJSON() throws IOException {
		Gson gson = new Gson();
	    String content = new String(Files.readAllBytes(Paths.get("songs.json")));
	    Type collectionType = new TypeToken<ObservableList<Song>>(){}.getType();
	    ArrayList<Song> songList = gson.fromJson(content, collectionType);
	    obsList = FXCollections.observableArrayList(songList);
	}
	
	
	//add song button clicked
	private void addProcedure(Stage mainStage) {
		modeLabel.setText("Add Mode");

	    disableButtons();
	    
	    doneButton.setOnAction(event -> {
	    	if (nameField.getText().trim().isEmpty() ||
	        	artistField.getText().trim().isEmpty()) {
	        	Alert alert = new Alert(AlertType.ERROR);
	        	      alert.initOwner(mainStage);
	        	      alert.setTitle("Blank Name or Artist");
	        	      alert.setHeaderText("Please enter both a song name and artist name.");
	        	      alert.showAndWait();
	        	      event.consume();
	        } else if (obsList.contains(new Song(nameField.getText(), artistField.getText()))) {
	        		Alert alert = new Alert(AlertType.ERROR);
	  		      		  alert.initOwner(mainStage);
	  		              alert.setTitle("Already Exists");
	  		              alert.setHeaderText("This song and artist are already in your library.");
	  		              alert.showAndWait();
	  		              event.consume();
	  		  //do not allow the user to enter a year that isnt an integer            
	        } else if(validYear(yearField.getText()) == false) {
	        	Alert alert = new Alert(AlertType.ERROR);
	      		  alert.initOwner(mainStage);
	              alert.setTitle("Invalid Year");
	              alert.setHeaderText("Please enter a valid year.");
	              alert.showAndWait();
	              event.consume();
	        }else {
	        	Song addedSong = new Song(nameField.getText(), 
		 			                      artistField.getText(),
		 			                      albumField.getText(),
		 			                      yearField.getText());
	        	
	        	Alert alert = new Alert(AlertType.CONFIRMATION);
				      alert.setTitle("Add New Song?");
				      alert.setHeaderText("Are you sure you want to add this song?");
				      alert.setContentText(delContentString(addedSong));
				Optional<ButtonType> result = alert.showAndWait();
				if (result.get() == ButtonType.OK) {
					if (obsList.isEmpty()) {
						obsList.add(addedSong);
						editButton.setDisable(false);
						deleteButton.setDisable(false);
					} else {
						obsList.add(addedSong);
					}
		        	
		        	listView.getSelectionModel()
		        	        .clearAndSelect(obsList.sorted()
		        	        		               .lastIndexOf(addedSong));
		        	try {
		        		writeJSON();
		        	} catch (Exception e) {
		        		System.out.println(e.toString());
		        	}
		        	cleanUpProcedure();
				}
	    }});
	    
	    //if user clicked cancel button
	    cancelButton.setOnAction(event -> {
	    	Alert alert = new Alert(AlertType.CONFIRMATION);
			  alert.setTitle("Cancel Adding Song?");
			  alert.setHeaderText("Are you sure you want to cancel adding this song?");
			  Optional<ButtonType> result = alert.showAndWait();
			  if (result.get() == ButtonType.OK) {
				cleanUpProcedure();
			  }
	    });
	}
	
	//edit button clicked
	private void editProcedure(Stage mainStage) {
		
		//get highlighted song
		Song item = listView.getSelectionModel().getSelectedItem();
		int index = obsList.lastIndexOf(item);
		
		//set up edit mode
		//grab names to edit from song details section
		modeLabel.setText("Edit Mode");
		nameField.setText(songNameLabel.getText());
	    artistField.setText(songArtistLabel.getText());
	    albumField.setText(songAlbumLabel.getText());
	    yearField.setText(songYearLabel.getText());
	    
	    //disable buttons to prevent unintended use
	    disableButtons();
	    
	    //handle done button actions
	    doneButton.setOnAction(event -> { 
	    	//if song or artist is empty
	    	if (nameField.getText().trim().isEmpty() ||
        		artistField.getText().trim().isEmpty()) {
        		Alert alert = new Alert(AlertType.ERROR);
        		      alert.initOwner(mainStage);
        		      alert.setTitle("Blank Name or Artist");
        		      alert.setHeaderText("Please enter both a song name and artist name.");
        		      alert.showAndWait();
        		      event.consume();
        		      //if song already exists
        	} else if (obsList.contains(new Song(nameField.getText(), artistField.getText())) && 
        			   !(nameField.getText().equals(item.getName()) && 
        				 artistField.getText().equals(item.getArtist()))) {
        		Alert alert = new Alert(AlertType.ERROR);
  		      		  alert.initOwner(mainStage);
  		              alert.setTitle("Already Exists");
  		              alert.setHeaderText("This song and artist are already in your library.");
  		              alert.showAndWait();
  		              event.consume();
        	 //do not allow the user to enter a year that isnt an integer            
        	} else if(validYear(yearField.getText()) == false) {
        	Alert alert = new Alert(AlertType.ERROR);
      		  alert.initOwner(mainStage);
              alert.setTitle("Invalid Year");
              alert.setHeaderText("Please enter a valid year.");
              alert.showAndWait();
              event.consume();
        	} else {
        		//add song to library
        		Song editedSong = new Song(nameField.getText(), 
										   artistField.getText(),
										   albumField.getText(),
										   yearField.getText());
        		Alert alert = new Alert(AlertType.CONFIRMATION);
				  alert.setTitle("Edit Song?");
				  alert.setHeaderText("Are these the changes you wish to make?");
				  alert.setContentText(delContentString(editedSong));
				  Optional<ButtonType> result = alert.showAndWait();
				  if (result.get() == ButtonType.OK) {
	        	obsList.set(index, editedSong);
	        	listView.getSelectionModel()
	        	        .select(editedSong);
	        	
	        	//set these details
	        	songAlbumLabel.setText(editedSong.getAlbum());
	        	songYearLabel.setText(editedSong.getYear());
	        	try {
	        		writeJSON();
	        	} catch (Exception e) {
	        		System.out.println(e.toString());
	        	}
	        	cleanUpProcedure();
				}
	    }});
	    
	    //if user cancels the add
	    cancelButton.setOnAction(event -> {
	    	Alert alert = new Alert(AlertType.CONFIRMATION);
			  alert.setTitle("Cancel Editing Song?");
			  alert.setHeaderText("Are you sure you want to cancel editing this song?");
			  Optional<ButtonType> result = alert.showAndWait();
			  if (result.get() == ButtonType.OK) {
				
				cleanUpProcedure();
			  }
	    });
	}
	
	//activate/de-activate buttons
	private void cleanUpProcedure() {
		modeLabel.setText("");
		nameField.setText("");
		nameField.setDisable(true);
		artistField.setText("");
	    artistField.setDisable(true);
	    albumField.setText("");
	    albumField.setDisable(true);
	    yearField.setText("");
	    yearField.setDisable(true);
	      
	    doneButton.setDisable(true);
	    cancelButton.setDisable(true);
	    
	    doneButton.setOnAction(null);
	    cancelButton.setOnAction(null);
	    
	  //if the list is empty and you cancel an add song
	  //need to make sure edit and delete are still disabled
	    if(obsList.isEmpty() == true) {
	    	addButton.setDisable(false);	
	    	editButton.setDisable(true);
	    	deleteButton.setDisable(true);
	    	listView.setDisable(false);
		 }
	    else {
	    	addButton.setDisable(false);
	    	editButton.setDisable(false);
	    	deleteButton.setDisable(false);
	    	listView.setDisable(false);
	    }
	}
	
	
	//disable buttons when in add or edit mode
	private void disableButtons() {
		//enable enter fields
		nameField.setDisable(false);
	    artistField.setDisable(false);
	    albumField.setDisable(false);
	    yearField.setDisable(false);
	    doneButton.setDisable(false);
	    cancelButton.setDisable(false);
		
	    //disable add edit and delete
		addButton.setDisable(true);
	    editButton.setDisable(true);
	    deleteButton.setDisable(true);
	    
	    //disable listview
	    listView.setDisable(true);
		
	}
	
	//action while pressing delete button
	private void showDeleteDialog(Stage mainStage) {
		Song item = listView.getSelectionModel().getSelectedItem();
		int index = listView.getSelectionModel().getSelectedIndex();
		if(index >= 0) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
			  alert.setTitle("Delete Song?");
			  alert.setHeaderText("Are you sure you want to delete this song?");
			  alert.setContentText(delContentString(item));
			  Optional<ButtonType> result = alert.showAndWait();
			  //if there is an item to delete, delete
			  if (result.get() == ButtonType.OK) {
				  //remove item
			      obsList.remove(item);
			      //handle where the list highlights after delete
			      //if it is not the last item, point to the song after it
			      //otherwise it will naturally point to the song before it
			      if(index < obsList.size()) {
			    	  listView.getSelectionModel().select(index);
			      }
			   //if is empty
			      if (obsList.isEmpty()) {
			    	  emptyProcedure();
			      }
			      //try to write to json file
			      try {
		        		writeJSON();
		          } catch (Exception e) {
		        		System.out.println(e.toString());
		          }
			  }
		}
	}
	
	
	private String delContentString(Song item) {
		String complete = "Song Name: " + item.getName() + "\n";
		complete = complete.concat("Artist: " + item.getArtist() + "\n");
		if (!(item.getAlbum().trim().isEmpty())) {
			complete = complete.concat("Album: " + item.getAlbum() + "\n");
		}
		if (!(item.getYear().trim().isEmpty())) {
			complete = complete.concat("Year: " + item.getYear() + "\n");
		}
		return complete;
	}
	
	
	//this method will check if the year is valid
	public static boolean validYear(String str)
	{
		//ignore empty string
		if(str.isEmpty() == true) {
			return true;
		}
		
		try {
			Integer.parseInt(str);
		} catch (NumberFormatException nfe) {
			return false;
		}
		//if it did not generate an exception then the string is an int
		return true;
	}
	
}




