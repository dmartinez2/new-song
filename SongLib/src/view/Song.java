//done by:
//David Martinez NetID: dam364
//Justin Hinds NetID: jth140

package view;

import java.util.Comparator;

public class Song implements Comparable<Song>{
	private String name;
	private String artist;
	private String album;
	private String year;
	
	public Song(String name, String artist, String album, String year) {
		this.name = name;
		this.artist = artist;
		this.album = album;
		this.year = year;
	}
	
	public Song(String name, String artist) {
		this.name = name;
		this.artist = artist;
		this.album = "";
		this.year = "";
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getNameUpper() {
		return this.name.toUpperCase();
	}
	
	public void setArtist(String artist) {
		this.artist = artist;
	}
	
	public String getArtist() {
		return this.artist;
	}
	
	public String getArtistUpper() {
		return this.artist.toUpperCase();
	}
	
	public void setAlbum(String album) {
		this.album = album;
	}
	
	public String getAlbum() {
		return this.album;
	}
	
	public void setYear(String year) {
		this.year = year;
	}
	
	public String getYear() {
		return this.year;
	}
	
	public int compareTo(Song s) {
		return Comparator.comparing(Song::getNameUpper)
				.thenComparing(Song::getArtistUpper)
				.compare(this, s);
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Song)) {
			return false;
		} else {
		Song s = (Song)o;
		return (this.getNameUpper().equals(s.getNameUpper()) && 
				this.getArtistUpper().equals(s.getArtistUpper()));
		}
	}
	
	public String toString() {
		return this.name + " by " + this.artist;
	}
}
